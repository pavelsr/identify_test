use utf8;
use strict;
use warnings;

use XPortal;
use cron::Pirates;

use Data::Dumper;

XPortal::DB::ConnectGlobal();
XPortal::DB::ConnectCrawler();

my $pirate = cron::Pirates->new(
  'master_url' => 'http://knizhnik.org',
  'site_id'    => 1,
  'debug'      => 1,
);

my $result = $pirate->IdentifyBook(
  'author' => 'Джоджо Мойес',
  'title'  => 'До встречи с тобой',
  'page'   => 'https://knizhnik.org/dzhodzho-mojes/do-vstrechi-s-toboj',
  'is_ebook' => 1,
  'local' => [ qw(local.txt) ],
);

warn Dumper($result);
